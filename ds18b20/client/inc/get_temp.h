/********************************************************************************
 *      Copyright:  (C) 2023 LXC
 *                  All rights reserved.
 *
 *       Filename:  get_temp.h
 *    Description:  This file 
 *
 *        Version:  1.0.0(07/05/23)
 *         Author:  Lin XinCheng <1481155734@qq.com>
 *      ChangeLog:  1, Release initial version on "07/05/23 19:14:01"
 *                 
 ********************************************************************************/



#ifndef _GET_TEMP__H_
#define _GET_TEMP__H_

int get_temp(float *temp);

#endif



